# AWS Cloudtrail

## Description

Upload the cloutrail information from the `prod` account to the `log` account.

<!-- BEGINNING OF PRE-COMMIT-TERRAFORM DOCS HOOK -->
## Requirements

| Name | Version |
|------|---------|
| <a name="requirement_terraform"></a> [terraform](#requirement\_terraform) | >= 0.13.0 |
| <a name="requirement_aws"></a> [aws](#requirement\_aws) | >= 2.0.0 |

## Providers

No providers.

## Modules

| Name | Source | Version |
|------|--------|---------|
| <a name="module_cloudtrail"></a> [cloudtrail](#module\_cloudtrail) | cloudposse/cloudtrail/aws | 0.20.1 |
| <a name="module_s3_bucket"></a> [s3\_bucket](#module\_s3\_bucket) | cloudposse/cloudtrail-s3-bucket/aws | 0.20.0 |

## Resources

No resources.

## Inputs

| Name | Description | Type | Default | Required |
|------|-------------|------|---------|:--------:|
| <a name="input_cloudtrail_bucket_tags"></a> [cloudtrail\_bucket\_tags](#input\_cloudtrail\_bucket\_tags) | Tags attached to Cloudtrail | `map(string)` | `{}` | no |
| <a name="input_cloudtrail_cloud_watch_logs_group_arn"></a> [cloudtrail\_cloud\_watch\_logs\_group\_arn](#input\_cloudtrail\_cloud\_watch\_logs\_group\_arn) | Specifies a log group name using an Amazon Resource Name (ARN), that represents the log group to which CloudTrail logs will be delivered | `string` | `""` | no |
| <a name="input_cloudtrail_cloud_watch_logs_role_arn"></a> [cloudtrail\_cloud\_watch\_logs\_role\_arn](#input\_cloudtrail\_cloud\_watch\_logs\_role\_arn) | Specifies the role for the CloudWatch Logs endpoint to assume to write to a user’s log group | `string` | `""` | no |
| <a name="input_cloudtrail_enable_log_file_validation"></a> [cloudtrail\_enable\_log\_file\_validation](#input\_cloudtrail\_enable\_log\_file\_validation) | Enable Cloudtrail log file integrity validation | `bool` | `false` | no |
| <a name="input_cloudtrail_enable_logging"></a> [cloudtrail\_enable\_logging](#input\_cloudtrail\_enable\_logging) | Enables logging for the trail. Setting this to false will pause logging. | `bool` | `true` | no |
| <a name="input_cloudtrail_event_selector"></a> [cloudtrail\_event\_selector](#input\_cloudtrail\_event\_selector) | Specifies an event selector for enabling data event logging. See: https://www.terraform.io/docs/providers/aws/r/cloudtrail.html for details on this variable | <pre>list(object({<br>    include_management_events = bool<br>    read_write_type           = string<br><br>    data_resource = list(object({<br>      type   = string<br>      values = list(string)<br>    }))<br>  }))</pre> | `[]` | no |
| <a name="input_cloudtrail_include_global_service_events"></a> [cloudtrail\_include\_global\_service\_events](#input\_cloudtrail\_include\_global\_service\_events) | Whether the trail is publishing events from global services such as IAM to the log files | `bool` | `true` | no |
| <a name="input_cloudtrail_is_multi_region_trail"></a> [cloudtrail\_is\_multi\_region\_trail](#input\_cloudtrail\_is\_multi\_region\_trail) | Whether the trail is created in the current region or in all regions | `bool` | `false` | no |
| <a name="input_cloudtrail_is_organization_trail"></a> [cloudtrail\_is\_organization\_trail](#input\_cloudtrail\_is\_organization\_trail) | The trail is an AWS Organizations trail | `bool` | `false` | no |
| <a name="input_cloudtrail_kms_key_arn"></a> [cloudtrail\_kms\_key\_arn](#input\_cloudtrail\_kms\_key\_arn) | Specifies the KMS key ARN to use to encrypt the logs delivered by CloudTrail | `string` | `""` | no |
| <a name="input_cloudtrail_name"></a> [cloudtrail\_name](#input\_cloudtrail\_name) | Name of the trail | `string` | n/a | yes |
| <a name="input_cloudtrail_sns_topic_name"></a> [cloudtrail\_sns\_topic\_name](#input\_cloudtrail\_sns\_topic\_name) | Specifies the name of the Amazon SNS topic defined for notification of log file delivery | `string` | `null` | no |
| <a name="input_namespace"></a> [namespace](#input\_namespace) | Namespace - usually abbreviation of your organization name. It's included in the ID element to ensure resource name globally unique. | `string` | n/a | yes |
| <a name="input_s3_bucket_abort_incomplete_multipart_upload_days"></a> [s3\_bucket\_abort\_incomplete\_multipart\_upload\_days](#input\_s3\_bucket\_abort\_incomplete\_multipart\_upload\_days) | Maximum time (in days) that you want to allow multipart uploads to remain in progress | `number` | `5` | no |
| <a name="input_s3_bucket_acl"></a> [s3\_bucket\_acl](#input\_s3\_bucket\_acl) | The canned ACL to apply. We recommend log-delivery-write for compatibility with AWS services | `string` | `"log-delivery-write"` | no |
| <a name="input_s3_bucket_allow_ssl_requests_only"></a> [s3\_bucket\_allow\_ssl\_requests\_only](#input\_s3\_bucket\_allow\_ssl\_requests\_only) | Set to `true` to require requests to use Secure Socket Layer (HTTPS/SSL). This will explicitly deny access to HTTP requests | `bool` | `false` | no |
| <a name="input_s3_bucket_block_public_acls"></a> [s3\_bucket\_block\_public\_acls](#input\_s3\_bucket\_block\_public\_acls) | Set to `false` to disable the blocking of new public access lists on the bucket | `bool` | `true` | no |
| <a name="input_s3_bucket_block_public_policy"></a> [s3\_bucket\_block\_public\_policy](#input\_s3\_bucket\_block\_public\_policy) | Set to `false` to disable the blocking of new public policies on the bucket | `bool` | `true` | no |
| <a name="input_s3_bucket_create_access_log_bucket"></a> [s3\_bucket\_create\_access\_log\_bucket](#input\_s3\_bucket\_create\_access\_log\_bucket) | A flag to indicate if a bucket for s3 access logs should be created | `bool` | `false` | no |
| <a name="input_s3_bucket_enable_glacier_transition"></a> [s3\_bucket\_enable\_glacier\_transition](#input\_s3\_bucket\_enable\_glacier\_transition) | Glacier transition might just increase your bill. Set to false to disable lifecycle transitions to AWS Glacier. | `bool` | `false` | no |
| <a name="input_s3_bucket_expiration_days"></a> [s3\_bucket\_expiration\_days](#input\_s3\_bucket\_expiration\_days) | Number of days after which to expunge the objects | `number` | `90` | no |
| <a name="input_s3_bucket_force_destroy"></a> [s3\_bucket\_force\_destroy](#input\_s3\_bucket\_force\_destroy) | A boolean that indicates all objects should be deleted from the bucket so that the bucket can be destroyed without error. These objects are not recoverable | `bool` | `false` | no |
| <a name="input_s3_bucket_glacier_transition_days"></a> [s3\_bucket\_glacier\_transition\_days](#input\_s3\_bucket\_glacier\_transition\_days) | Number of days after which to move the data to the glacier storage tier | `number` | `60` | no |
| <a name="input_s3_bucket_ignore_public_acls"></a> [s3\_bucket\_ignore\_public\_acls](#input\_s3\_bucket\_ignore\_public\_acls) | Set to `false` to disable the ignoring of public access lists on the bucket | `bool` | `true` | no |
| <a name="input_s3_bucket_kms_master_key_arn"></a> [s3\_bucket\_kms\_master\_key\_arn](#input\_s3\_bucket\_kms\_master\_key\_arn) | The AWS KMS master key ARN used for the SSE-KMS encryption. This can only be used when you set the value of sse\_algorithm as aws:kms. The default aws/s3 AWS KMS master key is used if this element is absent while the sse\_algorithm is aws:kms | `string` | `""` | no |
| <a name="input_s3_bucket_lifecycle_prefix"></a> [s3\_bucket\_lifecycle\_prefix](#input\_s3\_bucket\_lifecycle\_prefix) | Prefix filter. Used to manage object lifecycle events | `string` | `""` | no |
| <a name="input_s3_bucket_lifecycle_rule_enabled"></a> [s3\_bucket\_lifecycle\_rule\_enabled](#input\_s3\_bucket\_lifecycle\_rule\_enabled) | Enable lifecycle events on this bucket | `bool` | `true` | no |
| <a name="input_s3_bucket_lifecycle_tags"></a> [s3\_bucket\_lifecycle\_tags](#input\_s3\_bucket\_lifecycle\_tags) | Tags filter. Used to manage object lifecycle events | `map(string)` | `{}` | no |
| <a name="input_s3_bucket_name"></a> [s3\_bucket\_name](#input\_s3\_bucket\_name) | Name of S3 bucket to store Cloudtrail logs | `string` | n/a | yes |
| <a name="input_s3_bucket_notifications_enabled"></a> [s3\_bucket\_notifications\_enabled](#input\_s3\_bucket\_notifications\_enabled) | Send notifications for the object created events. Used for 3rd-party log collection from a bucket. This does not affect access log bucket created by this module. To enable bucket notifications on the access log bucket, create it separately using the cloudposse/s3-log-storage/aws | `bool` | `false` | no |
| <a name="input_s3_bucket_restrict_public_buckets"></a> [s3\_bucket\_restrict\_public\_buckets](#input\_s3\_bucket\_restrict\_public\_buckets) | Set to `false` to disable the restricting of making the bucket public | `bool` | `true` | no |
| <a name="input_s3_bucket_tags"></a> [s3\_bucket\_tags](#input\_s3\_bucket\_tags) | Tags attached to S3 bucket | `map(string)` | `{}` | no |
| <a name="input_stage"></a> [stage](#input\_stage) | Stage - usually indicate role e.g. 'prod', 'staging' . It's included in the ID element to ensure resource name globally unique. | `string` | n/a | yes |

## Outputs

| Name | Description |
|------|-------------|
| <a name="output_cloudtrail_arn"></a> [cloudtrail\_arn](#output\_cloudtrail\_arn) | ARN of the created Cloudtrail resource |
| <a name="output_cloudtrail_home_region"></a> [cloudtrail\_home\_region](#output\_cloudtrail\_home\_region) | The region in which the Cloudtrail resource was created |
| <a name="output_cloudtrail_id"></a> [cloudtrail\_id](#output\_cloudtrail\_id) | Name of the created Cloudtrail resource |
| <a name="output_s3_bucket_arn"></a> [s3\_bucket\_arn](#output\_s3\_bucket\_arn) | ARN of the S3 bucket storing Cloudtrail log |
| <a name="output_s3_bucket_domain_name"></a> [s3\_bucket\_domain\_name](#output\_s3\_bucket\_domain\_name) | FQDN of the S3 bucket storing Cloudtrail log |
| <a name="output_s3_bucket_id"></a> [s3\_bucket\_id](#output\_s3\_bucket\_id) | ID of the S3 bucket storing Cloudtrail log |
| <a name="output_s3_bucket_notifications_sqs_queue_arn"></a> [s3\_bucket\_notifications\_sqs\_queue\_arn](#output\_s3\_bucket\_notifications\_sqs\_queue\_arn) | Notifications SQS queue ARN of the S3 bucket storing Cloudtrail log |
| <a name="output_s3_bucket_prefix"></a> [s3\_bucket\_prefix](#output\_s3\_bucket\_prefix) | Prefix configured for lifecycle rules of the S3 bucket storing Cloudtrail log |
<!-- END OF PRE-COMMIT-TERRAFORM DOCS HOOK -->


## Run project with Terraform Commands

### I. Requirements

- Have aws-vault installed

- Have terraform cli installed
- Have aws-vault installed, see guide [here](https://github.com/99designs/aws-vault#:~:text=AWS%20Vault%20is%20a%20tool,to%20your%20shell%20and%20applications.).
- Setup aws-vault to have aws account profile
- Having sufficent roles/permissions setup to allow IAM user to assume role in prod account and log account

### II. Command terraform commands

1. Init project

  ```bash
  aws-vault exec keltio --no-session -- terraform init
  ```

2. Generate plan

  ```bash
  aws-vault exec keltio --no-session -- terraform plan
  ```

3. Apply terraform plan

  ```bash
  aws-vault exec keltio --no-session -- terraform apply --auto-approve
  ```

4. Destroy resources

```bash
aws-vault exec keltio --no-session -- terraform destroy --auto-approve
```
